import { Ingredient } from './../shared/ingredient.model';
import { Subject } from 'rxjs/Subject';
export class ShopplingListService {
    ingredientsChanged = new Subject<Ingredient[]>();
    startedEditing = new Subject<number>();
    private ingredients: Ingredient[] = [
        new Ingredient('Apples', 50),
        new Ingredient('Oranges', 10)
      ];
    getIngredients() {
        return this.ingredients.slice();
    }

    addIngredient(ingred: Ingredient) {
        this.ingredients.push(ingred);
        this.ingredientsChanged.next(this.ingredients.slice());
    }

    addIngredients(ingreds: Ingredient[]) {
        this.ingredients.push(...ingreds);
        this.ingredientsChanged.next(this.ingredients.slice());
    }

    getIngredient(index: number) {
      return this.ingredients[index];
    }

    updateIngredient(index: number, newIngredient: Ingredient) {
      this.ingredients[index] = newIngredient;
      this.ingredientsChanged.next(this.ingredients.slice());
    }

    deleteIngredient(index: number) {
      this.ingredients.splice(index, 1);
      this.ingredientsChanged.next(this.ingredients.slice());
    }
}
