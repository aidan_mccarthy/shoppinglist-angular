import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  loadedFeature = 'recipe';

  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDW4GvKwaGrocIhwTOhhyCxL3NkOOuBgYc',
      authDomain: 'ng-recipe-book-e39c0.firebaseapp.com',
      databaseURL: 'https://ng-recipe-book-e39c0.firebaseio.com',
      projectId: 'ng-recipe-book-e39c0',
      storageBucket: 'ng-recipe-book-e39c0.appspot.com',
      messagingSenderId: '267721193525'
    });
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }


}
