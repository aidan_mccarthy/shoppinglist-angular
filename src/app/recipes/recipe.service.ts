import { Subject } from 'rxjs/Subject';
import { ShopplingListService } from './../shopping-list/shopping-list.service';
import { Ingredient } from './../shared/ingredient.model';
import { Recipe } from './recipes.model';
import { Injectable } from '@angular/core';
@Injectable()

export class RecipeService {

  recipeChanged = new Subject<Recipe []>();
    private recipes: Recipe[] = [
        new Recipe('Chicken Pizza', 'Just a plain chicken pizza',
        'https://www.tasteofhome.com/wp-content/uploads/2017/10/Chicken-Pizza_exps30800_FM143298B03_11_8bC_RMS-2-696x696.jpg',
        [
            new Ingredient('Pizza', 1),
            new Ingredient('Chicken', 2),

        ]),
        new Recipe('Ham and Pineapple pizza', 'the devils pizza',
        'https://img.taste.com.au/WcEYk0xM/w720-h480-cfill-q80/taste/2016/11/bacon-cheddar-and-pineapple-pizza-77250-1.jpeg',
        [
            new Ingredient('Pizza', 1),
            new Ingredient('Pineapple', 2),
            new Ingredient('Ham', 3),
        ])
      ];

    constructor(private slService: ShopplingListService) {}

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index: number) {
      return this.recipes[index];
    }

    addIngredientsToShoppingList(ingreds: Ingredient[]) {
        this.slService.addIngredients(ingreds);
    }

    addRecipe(recipe: Recipe) {
      this.recipes.push(recipe);
      this.recipeChanged.next(this.recipes.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe) {
      this.recipes[index] = newRecipe;
      this.recipeChanged.next(this.recipes.slice());
    }

    deleteRecipe(index: number) {
      this.recipes.splice(index, 1);
      this.recipeChanged.next(this.recipes.slice());
    }

    setRecipes(recipes: Recipe[]) {
      this.recipes = recipes;
      this.recipeChanged.next(this.recipes.slice());
    }
}
